/* eslint-disable camelcase */
import Vue from 'vue'

export default class VuexJsonQLTools {
  constructor (dataFields) {
    // TODO: make data structure scheme
    // TODO: refactor data to store
    // TODO: ??? decide problem with more than one object of collection (ex. expert and sender from people)
    this.dataFields = dataFields
  }

  arrayToMap (array) {
    return Object.fromEntries(array.map(el => [el.id, el]))
  }

  stringToCamelCaseStartedUpCase (text) {
    return text.toLowerCase().split(/[\s,\-_]+/)
      .map(word => word.replace(word[0], word[0].toUpperCase()))
      .join('')
  }

  stringToCamelCaseStartedLowCase (text) {
    return text.toLowerCase().split(/[\s,\-_]+/).map((word, index) => {
      return index === 0 ? word : word.replace(word[0], word[0].toUpperCase())
    }).join('')
  }

  stringToUpperCase (text, splitter = '') {
    return text.toUpperCase().split(/[\s,\-_]+/).join(splitter)
  }

  getDataField (itemName) {
    let dataField = null
    this.dataFields.forEach(_dataField => {
      if (_dataField.item_name === itemName || _dataField.items_name === itemName) {
        dataField = _dataField
      }
    })
    return dataField
  }

  makeState () {
    const low = this.stringToCamelCaseStartedLowCase
    const generated = {}
    this.dataFields.forEach(dataField => {
      generated[`${low(dataField.items_name)}`] = {}
      if (!dataField.fields) {
        return
      }
      Object.keys(dataField.fields).forEach(field => {
        generated[`${low(dataField.items_name)}_${low(field)}`] = {}
      })
      generated[`${low(dataField.items_name)}_last`] = {}
    })
    return generated
  }

  makeGetters () {
    const low = this.stringToCamelCaseStartedLowCase

    const makeGetterCommon = stateName => state => Object.values(state[stateName])

    const makeGetterById = stateName => state => id => state[stateName][String(id)]

    const makeGetterLast = stateName => state => state[`${stateName}_last`]

    const makeGetterByLast = stateName => state => state[stateName][String(state[`${stateName}_last`])]

    const makeGetterByIdCurrent = (stateName, authDependencyField) => (state, getters, rootState) =>
      state[stateName][rootState.auth[authDependencyField]]

    const makeGetterByIdFromRouteWithAuthField = (stateName, authDependencyField) => (state, getters, rootState) =>
      !rootState.route.params || !rootState.route.params.id
        ? state[stateName][rootState.auth[authDependencyField]]
        : state[stateName][rootState.route.params.id]

    const makeGetterByIdFromRoute = stateName => (state, getters, rootState) =>
      !rootState.route.params || !rootState.route.params.id
        ? null
        : state[stateName][rootState.route.params.id]

    // TODO: getData with configuring right get method name by data structure scheme
    //  ({ filter = null, query = null, isOne = false, pagination = null })
    const generated = {
      getData: () => {}
    }
    // TODO: universal getter by JsonQL query
    this.dataFields.forEach(dataField => {
      generated[`get_${low(dataField.items_name)}`] = makeGetterCommon(low(dataField.items_name))

      generated[`get_${low(dataField.items_name)}_by_id`] = makeGetterById(low(dataField.items_name))

      generated[`get_${low(dataField.items_name)}_last`] = makeGetterLast(low(dataField.items_name))

      generated[`get_${low(dataField.items_name)}_by_last`] = makeGetterByLast(low(dataField.items_name))

      const genGetterName = (field, postfix) => `get_${low(field)}_by_${low(dataField.item_name)}_${postfix}`

      const genStateName = field => `${low(dataField.items_name)}_${low(field)}`

      if (!dataField.fields) {
        return
      }

      Object.keys(dataField.fields).forEach(field => {
        generated[genGetterName(field, 'id')] = makeGetterById(genStateName(field))
      })

      if (dataField.authDependencyField) {
        generated[`get_${low(dataField.items_name)}_by_idFromRoute`] =
          makeGetterByIdFromRouteWithAuthField(low(dataField.items_name), dataField.authDependencyField)

        generated[`get_${low(dataField.items_name)}_by_idCurrent`] =
          makeGetterByIdCurrent(low(dataField.items_name), dataField.authDependencyField)

        Object.keys(dataField.fields).forEach(field => {
          // TODO: rethink by smth id getters

          generated[genGetterName(field, 'idFromRoute')] =
            makeGetterByIdFromRouteWithAuthField(genStateName(field), dataField.authDependencyField)

          generated[genGetterName(field, 'idCurrent')] =
            makeGetterByIdCurrent(genStateName(field), dataField.authDependencyField)
        })
      } else {
        generated[`get_${low(dataField.items_name)}_by_idFromRoute`] =
          makeGetterByIdFromRoute(low(dataField.items_name))

        Object.keys(dataField.fields).forEach(field => {
          generated[genGetterName(field, 'idFromRoute')] =
            makeGetterByIdFromRoute(genStateName(field))
        })
      }
    })
    return generated
  }

  updateOtherStates (state, dataField, payload) {
  this.dataFields.forEach(_dataField => {
      if (!_dataField || !_dataField.fields || !_dataField.fields[dataField.items_name]) {
        return
      }
      const s = state[`${_dataField.items_name}_${dataField.items_name}`]
      if (!s) {
        return
      }
      Object.keys(s).forEach(key => {
        let isUpdated = false
        s[key].forEach((el, index) => {
          if (String(el.id) === String(payload.id)) {
            Vue.set(s[key], index, payload)
            isUpdated = true
          }
        })
        if (!isUpdated) {
          s[key].push(payload)
        }
      })
    })
  }

  deleteOtherStates (state, dataField, payload) {
    this.dataFields.forEach(_dataField => {
      if (!_dataField || !_dataField.fields || !_dataField.fields[dataField.items_name]) {
        return
      }
      const s = state[`${_dataField.items_name}_${dataField.items_name}`]
      if (!s) {
        return
      }
      Object.keys(s).forEach(key => {
        s[key].forEach((el, index) => {
          if (String(el.id) === String(payload.id)) {
            Vue.set(s[key], index, null)
          }
        })
      })
    })
  }

  makeMutations () {
    const up = this.stringToUpperCase
    const lowCamel = this.stringToCamelCaseStartedLowCase
    const generated = {}
    this.dataFields.forEach(dataField => {
      const genStateName = field => `${lowCamel(dataField.items_name)}_${lowCamel(field)}`

      generated[`FETCH_${up(dataField.items_name)}`] = (state, payload) => {
        Vue.set(state, dataField.items_name, payload)
      }

      generated[`FETCH_${up(dataField.items_name)}_BY_ID`] = (state, payload) => {
        Vue.set(state[dataField.items_name], String(payload.id), payload)
      }

      generated[`ADD_${up(dataField.items_name)}_BY_ID`] = (state, payload) => {
        Vue.set(state[dataField.items_name], String(payload.id), payload)
        Vue.set(state, `${dataField.items_name}_last`, String(payload.id))
        this.updateOtherStates(state, dataField, payload)
      }

      generated[`UPDATE_${up(dataField.items_name)}_BY_ID`] = (state, payload) => {
        Vue.set(state[dataField.items_name], String(payload.id), payload)
        Vue.set(state, `${dataField.items_name}_last`, String(payload.id))
        this.updateOtherStates(state, dataField, payload)
      }

      generated[`DELETE_${up(dataField.items_name)}_BY_ID`] = (state, payload) => {
        Vue.set(state[dataField.items_name], String(payload.id), null)
        Vue.set(state, `${dataField.items_name}_last`, null)
        this.deleteOtherStates(state, dataField, payload)
      }

      if (!dataField.fields) {
        return
      }

      Object.keys(dataField.fields).forEach(field => {
        generated[`FETCH_${up(dataField.items_name)}_${up(field)}`] =
          (state, payload) =>
            Vue.set(state, genStateName(field), payload)

        generated[`FETCH_${up(dataField.items_name)}_${up(field)}_BY_ID`] =
          (state, payload) =>
            Vue.set(state[genStateName(field)], String(payload.id), payload)

        let references = []
        if (dataField.fields[field].reference_item) {
          references = [dataField.fields[field].reference_item]
        }
        if (dataField.fields[field].reference_items) {
          references = dataField.fields[field].reference_items
        }
        if (references.length) {
          // TODO: mb make one mutation with all data items ?
          references.forEach(reference => {
            generated[`FETCH_${up(dataField.items_name)}_${up(field)}_BY_${up(reference)}`] =
              (state, payload) =>
                Object.keys(payload).forEach(key => Vue.set(state[genStateName(field)], String(key), payload[key]))
          })
        }
      })
    })
    return generated
  }

  makeActions (service) {
    const up = this.stringToUpperCase
    const lowCamel = this.stringToCamelCaseStartedLowCase
    const generated = {
      FETCH_DATA: async ({ rootState, commit, dispatch }, rootQuery) => {
        Object.keys(rootQuery).forEach(key => {
          // TODO: check arguments
          const { filter = null, isOne = false, pagination = null } = rootQuery[key]
          const { from, by, orig, query = null } = rootQuery[key]
          const { use_route_field_as_orig, use_auth_field_as_orig } = rootQuery[key]
          const collection = from || key
          const dataField = this.getDataField(collection)
          let param = null
          // TODO: error processing ?
          if (use_route_field_as_orig && rootState.route && rootState.route.params && rootState.route.params[use_route_field_as_orig]) {
            param = rootState.route.params[use_route_field_as_orig]
          } else if (use_auth_field_as_orig && rootState.auth) {
            param = rootState.auth[use_auth_field_as_orig]
          } else if (orig !== undefined) {
            param = orig
          } else {
            // TODO: error
            console.log('Error')
          }
          // TODO: validation id
          // TODO: dispatch to other action
          // TODO: fix commit
          const newFilter = by ? { [by]: String(param) } : null
          const items = service[dataField.items_name].get({
            query,
            filter: {
              ...filter,
              ...newFilter
            },
            isOne,
            pagination
          })
          // TODO: don't save referenced fields (ex. expertises, projectsParticipation)
          // saving info
          // commit(`FETCH_${up(dataField.items_name)}_${up(dataField.items_name)}INFO_BY_ID`,
          //   Object.fromEntries(items.map(el => [el.id, el])))
          // TODO: refactor "FETCH_<smth>_<smth>INFO" to "FETCH_<smth>"
          items.forEach(item => {
            commit(`FETCH_${up(dataField.items_name)}_BY_ID`, item)
          })
          // saving references data
          if (query) {
            Object.keys(query).forEach(nestedKey => {
              // TODO: split data to different local storages
              // array => Object.fromEntries(array.map(el => [el.id, el]))
              if (!query[nestedKey].by) {
                // TODO: ???
                if (process.env.NODE_ENV !== 'production') {
                  console.error('[warn]: Error in JsonQLVuexHelpers.makeActions. "by" does not exist in query: ', query[nestedKey])
                }
                return
              }
              if (!query[nestedKey].orig) {
                // TODO: ???
                if (process.env.NODE_ENV !== 'production') {
                  console.error('[warn]: Error in JsonQLVuexHelpers.makeActions. "orig" does not exist in query: ', query[nestedKey])
                }
                return
              }
              commit(`FETCH_${up(dataField.items_name)}_${up(query[nestedKey].from || nestedKey)}_BY_${up(query[nestedKey].by)}`,
                Object.fromEntries(items.map(el => [el[query[nestedKey].orig], el[query[nestedKey].as || nestedKey]])))
            })
          }
        })
      },
      ADD_DATA: async ({ rootState, commit, dispatch }, rootQuery) => {
        const item = service[rootQuery.collection].add(rootQuery.item)
        commit(`ADD_${up(rootQuery.collection)}_BY_ID`, item)
      },
      UPDATE_DATA: async ({ rootState, commit, dispatch }, rootQuery) => {
        const item = service[rootQuery.collection].update(rootQuery.item)
        commit(`UPDATE_${up(rootQuery.collection)}_BY_ID`, item)
      },
      DELETE_DATA: async ({ rootState, commit, dispatch }, rootQuery) => {
        const item = service[rootQuery.collection].delete(rootQuery.item)
        commit(`DELETE_${up(rootQuery.collection)}_BY_ID`, item)
      }
    }
    // TODO: check
    this.dataFields.forEach(dataField => {
      generated[`FETCH_${up(dataField.items_name)}`] = async ({ commit }) => {
        const payload = await service[dataField.items_name].get({})
        if (!payload || !payload.length) {
          return
        }
        commit(`FETCH_${up(dataField.items_name)}`, this.arrayToMap(payload))
      }
      generated[`FETCH_${up(dataField.items_name)}_BY_ID`] = async ({ commit }, id) => {
        const payload = await service[dataField.items_name].getById(id)
        if (!payload) {
          return
        }
        commit(`FETCH_${up(dataField.items_name)}_BY_ID`, payload)
      }
      // FIXME
      // if (!dataField.fields) {
      //   return
      // }
      // Object.keys(dataField.fields).forEach(field => {
      //   generated[`FETCH_${up(dataField.items_name)}_${up(field)}`] = async ({ commit, state, getters }) => {
      //     const payload = await service[`${lowCamel(dataField.items_name)}_${lowCamel(field)}`]
      //     if (!payload || !payload.length) {
      //       return
      //     }
      //     commit(`FETCH_${up(dataField.items_name)}_${up(field)}`, this.arrayToMap(payload))
      //   }
      //   generated[`FETCH_${up(dataField.items_name)}_${up(field)}_BY_ID`] = async ({ commit, state, getters }, id) => {
      //     const payload = await service[`${lowCamel(dataField.items_name)}_${lowCamel(field)}`][String(id)]
      //     if (!payload) {
      //       return
      //     }
      //     commit(`FETCH_${up(dataField.items_name)}_${up(field)}_BY_ID`, payload)
      //   }
      // })
    })
    return generated
  }
}
