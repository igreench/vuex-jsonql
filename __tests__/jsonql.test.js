import Vue from 'vue'
import Vuex from 'vuex'
import JsonQLService from 'jsonql-service'
import VuexJsonQLTools from '../src'

describe('Vuex JsonQL Tools', () => {

  Vue.use(Vuex)

  const dataFields = [
    {
      item_name: 'project',
      items_name: 'projects',
      fields: {
        projects_participation: {
          reference_item: 'project_id'
        },
        requests: {
          reference_items: ['requester_id', 'responder_id', 'project_id']
        }
      }
    },
    {
      item_name: 'person',
      items_name: 'people',
      authDependencyField: 'currentUserId',
      fields: {
        projects_participation: {
          reference_item: 'person_id'
        },
        requests: {
          reference_items: ['requester_id', 'responder_id', 'person_id']
        }
      }
    },
    {
      item_name: 'request',
      items_name: 'requests'
    }
  ]

  const people = [
    {
      id: 0,
      name: 'Peter'
    },
    {
      id: 1,
      name: 'John'
    }
  ]

  const projects = [
    {
      id: 0,
      name: 'Project0'
    },
    {
      id: 1,
      name: 'Project1'
    }
  ]

  const projectsParticipation = [
    {
      id: 0,
      project_id: 0,
      person_id: 0
    },
    {
      id: 1,
      project_id: 1,
      person_id: 1
    }
  ]

  const requests = [
    {
      id: 0,
      requester_id: 0,
      requester_type: 0,
      responder_id: 0,
      responder_type: 1,
      status: 0
    },
    {
      id: 1,
      requester_id: 1,
      requester_type: 1,
      responder_id: 1,
      responder_type: 0,
      status: 1
    }
  ]

  const tools = new VuexJsonQLTools(dataFields)

  const peopleService = new JsonQLService(people)
  const projectsService = new JsonQLService(projects)
  const projectsParticipationService = new JsonQLService(projectsParticipation)
  const requestsService = new JsonQLService(requests)

  const dataService = {
    people: peopleService,
    projects: projectsService,
    projectsParticipation: projectsParticipationService,
    requests: requestsService
  }

  peopleService.setRootService(dataService)
  projectsService.setRootService(dataService)
  projectsParticipationService.setRootService(dataService)
  requestsService.setRootService(dataService)

  test('Store', async () => {
    const actions = tools.makeActions(dataService)

    expect(!!actions.FETCH_PROJECTS_BY_ID).toBe(true)

    const store = new Vuex.Store({
      state: tools.makeState(),
      getters: tools.makeGetters(),
      mutations: tools.makeMutations(),
      actions
    })

    expect(!!store.state.projects_projectsParticipation).toBe(true)

    await store.dispatch('FETCH_PROJECTS_BY_ID', 0)

    expect(store.state.projects['0'].name).toBe('Project0')

    await store.dispatch('FETCH_DATA', {
      people: {
        by: 'id',
        orig: 0,
        filter: {},
        pagination: {},
        query: {
          projectsParticipation: {
            by: 'person_id',
            orig: 'id',
            query: {
              project: {
                from: 'projects',
                by: 'id',
                orig: 'project_id',
                isOne: true
              }
            }
          }
        }
      }
    })

    expect(store.state.people['0'].projectsParticipation[0].project.name).toBe('Project0')
  })
})
